// self invoking function or a constructor
(function(){
    "use strict";
    let student1 = {name: "Joe", gender: "M", gpa: 3.5};
    console.log(student1);
    function Student(name, gender, gpa){
        this.name = name;
        this.gender = gender;
        this.gpa = gpa;
    }
    let student2 = new Student("Susan", "F", 3.5);
    let student3 = new Student("Susan", "F", 3.5);
    let student4 = new Student("Susan", "F", 1.5);
    let classroom = [student1, student2, student3, student4];
    console.log(classroom);

    let attendance = classroom.map(student => student.name);
    console.log(attendance);

    let average = (classroom.reduce((sum, student) => sum + student.gpa, 0))/classroom.length;
    console.log(average);

    let failing = classroom.filter(student => student.gpa<2.0);
    console.log(failing);
})();




// let classroom = [{id:123, name:'Bob', gender:'M', gpa: 3.4},
//                 {id:124, name:'Joe', gender:'M', gpa:1.7},
//                 {id:125, name:'Susan', gender:'F', gpa:3.6},
//                 {id:126, name:'Craig', gender:'M', gpa:2.7},
//                 {id:127, name:'Mary', gender:'F', gpa:4.0}];
//
// let nameList = classroom.map(student => student.name);
//
// console.log(nameList);