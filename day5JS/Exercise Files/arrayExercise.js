"use strict";

let week = ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday"];
console.log(week);

week.unshift("Sunday");
console.log(week);

week.push("Saturday");
console.log(week);

let pos = week.indexOf('Monday');
week.splice(pos,1);
console.log(week);

let weekNew = week.slice(0,3);
week.splice(0,3);
console.log(weekNew);
console.log(week);

let madHatter = ["Hatter"];
madHatter.unshift("Mad");
let combine = madHatter.join("");
console.log(combine);

// Instructor code
console.log("Mad" + "Hatter");
let word = "Hatter";
word = word.replace("H", "Mad H");
console.log(word);

console.log(word.split(" "));

console.log(word[6]);
//

combine = madHatter.join(" ");
console.log(combine);
console.log(combine.charAt(1));