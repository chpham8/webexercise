"use strict";

function Student(id, name, gender, gpa){
    this.id = id;
    this.name = name;
    this.gender = gender;
    this. gpa = gpa;
}

let student1 = new Student(1, "Susan", "F", 3.9);
let student2 = new Student(2, "George", "M", 2.1);
let student3 = new Student(3, "Cindy", "F", 3.8);
let student4 = new Student(4, "Joe", "M", 1.8);
let student5 = new Student(5, "Bob", "M", 3.2);

let classroom = [student1, student2, student3, student4, student5];
console.log(classroom);

console.log(classroom[1].name);

let nameList = classroom.map(student => student.name);
console.log(nameList);

// Condition ? True code : false code
// instructor code
//let highestGPA = classroom.reduce((max, student) => student.gpa > max ? max = student.gpa : max, 0);
let highestGPA = classroom.reduce((highest, student) => (highest.gpa || 0) > student.gpa ? highest : student);

//
// let highestGPA = classroom.reduce(function (highest, classroom) {
//     return (highest.gpa || 0) > classroom.gpa ? highest : classroom;
// }, {});

console.log(highestGPA);

let males = classroom.filter(student => student.gender === "M");
console.log(males);

let females = classroom.filter(student => student.gender === "F");
console.log(females);